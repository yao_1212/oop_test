<?php 
//抽象產品角色
abstract class PeopleUnit
{
	public abstract function playSlogan();
}

//具體產品角色
class PeopleWorker extends PeopleUnit
{
	public function playSlogan()
	{
		echo "產出工人yaya<br>--------------------------<br>";
	}
}

//具體產品角色
class PeopleSolider extends PeopleUnit
{
	public function playSlogan()
	{
		echo "產出士兵GGGGGGG<br>--------------------------<br>";
	}
}

//具體產品角色
class PeopleAirplant extends PeopleUnit
{
	public function build()
	{
		echo "組裝中....<br>";
	}

	public function playSlogan()
	{
		echo "產出飛機~~~<br>--------------------------<br>";
	}
}

// ===============================================================






//抽象產品角色
abstract class BugUnit
{
	public abstract function shout();
}

//具體產品角色
class BugWorker extends BugUnit
{
	public function shout()
	{
		echo "產出蟲蟲yaya<br>--------------------------<br>";
	}
}

//具體產品角色
class BugSolider extends BugUnit
{
	public function shout()
	{
		echo "產出蟲兵GGGGGGG<br>--------------------------<br>";
	}
}

//具體產品角色
class BugAirplant extends BugUnit
{
	public function shout()
	{
		echo "產出飛天蟲~~<br>--------------------------<br>";
	}
}

// ===============================================================










abstract class BuildFactory
{
	abstract function createPeopleUnit();
	abstract function createBugUnit();	
}

class WorkerCenter extends BuildFactory
{
	public function createPeopleUnit()
	{
		return new PeopleWorker;
	}
	public function createBugUnit()
	{
		return new BugWorker;
	}
}

class SoliderCenter extends BuildFactory
{
	public function createPeopleUnit()
	{
		return new PeopleSolider;
	}
	public function createBugUnit()
	{
		return new BugSolider;
	}
}

class AirplantCenter extends BuildFactory
{
	public function createPeopleUnit()
	{
		$Airplant = new PeopleAirplant;
		$Airplant->build();
		return new PeopleAirplant;
	}
	public function createBugUnit()
	{
		$Airplant = new BugAirplant;
		return new BugAirplant;
	}
}





